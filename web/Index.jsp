<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Document   : Index.jsp
    Created on : Apr 1, 2009, 10:39:48 AM
    Author     : John Yeary
-->
<jsp:root version="2.1" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/webui/webuijsf">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>
        <webuijsf:page id="page1">
            <webuijsf:html id="html1">
                <webuijsf:head id="head1" title="Index">
                    <webuijsf:link id="link1" url="/resources/stylesheet.css"/>
                </webuijsf:head>
                <webuijsf:body id="body1" style="-rave-layout: grid">
                    <webuijsf:form id="form1">
                        <webuijsf:table augmentTitle="false" id="table1" style="left: 24px; top: 24px; position: absolute; width: 450px" title="Table" width="0">
                            <webuijsf:tableRowGroup id="tableRowGroup1" rows="10" sourceData="#{Index.productDataProvider}" sourceVar="currentRow" styleClasses="odd-row,even-row">
                                <webuijsf:tableColumn headerText="PRODUCT_ID" id="tableColumn1" sort="PRODUCT.PRODUCT_ID">
                                    <webuijsf:staticText id="staticText1" text="#{currentRow.value['PRODUCT.PRODUCT_ID']}"/>
                                </webuijsf:tableColumn>
                                <webuijsf:tableColumn headerText="MANUFACTURER_ID" id="tableColumn2" sort="PRODUCT.MANUFACTURER_ID">
                                    <webuijsf:staticText id="staticText2" text="#{currentRow.value['PRODUCT.MANUFACTURER_ID']}"/>
                                </webuijsf:tableColumn>
                                <webuijsf:tableColumn headerText="PRODUCT_CODE" id="tableColumn3" sort="PRODUCT.PRODUCT_CODE">
                                    <webuijsf:staticText id="staticText3" text="#{currentRow.value['PRODUCT.PRODUCT_CODE']}"/>
                                </webuijsf:tableColumn>
                                <webuijsf:tableColumn headerText="PURCHASE_COST" id="tableColumn4" sort="PRODUCT.PURCHASE_COST">
                                    <webuijsf:staticText id="staticText4" text="#{currentRow.value['PRODUCT.PURCHASE_COST']}"/>
                                </webuijsf:tableColumn>
                                <webuijsf:tableColumn headerText="QUANTITY_ON_HAND" id="tableColumn5" sort="PRODUCT.QUANTITY_ON_HAND">
                                    <webuijsf:staticText id="staticText5" text="#{currentRow.value['PRODUCT.QUANTITY_ON_HAND']}"/>
                                </webuijsf:tableColumn>
                                <webuijsf:tableColumn headerText="MARKUP" id="tableColumn6" sort="PRODUCT.MARKUP">
                                    <webuijsf:staticText id="staticText6" text="#{currentRow.value['PRODUCT.MARKUP']}"/>
                                </webuijsf:tableColumn>
                                <webuijsf:tableColumn headerText="AVAILABLE" id="tableColumn7" sort="PRODUCT.AVAILABLE">
                                    <webuijsf:staticText id="staticText7" text="#{currentRow.value['PRODUCT.AVAILABLE']}"/>
                                </webuijsf:tableColumn>
                                <webuijsf:tableColumn headerText="DESCRIPTION" id="tableColumn8" sort="PRODUCT.DESCRIPTION">
                                    <webuijsf:staticText id="staticText8" text="#{currentRow.value['PRODUCT.DESCRIPTION']}"/>
                                </webuijsf:tableColumn>
                            </webuijsf:tableRowGroup>
                        </webuijsf:table>
                    </webuijsf:form>
                </webuijsf:body>
            </webuijsf:html>
        </webuijsf:page>
    </f:view>
</jsp:root>
